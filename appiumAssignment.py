import unittest
from appium import webdriver
desired_cap = {
  "platformName": "Android",
  "deviceName": "emulator-5554",
  "app": "C:\\Users\\ashvini.local\\Downloads\\com.flipkart.android.1200017.apk",
  "automationName": "uiautomator2"
}

driver = webdriver.Remote('http://localhost:4723/wi/hub',desired_cap)
driver.implicitly_wait(30)

#firstTC : handling popup
driver.find_element_by_id("com.flipkart.android:id/btn_skip").click()

#2ndTc:interact with textbox
driver.find_element_by_id("com.flipkart.android:id/search_widget_textbox").click()
driver.find_element_by_id("com.flipkart.android:id/search_autoCompleteTextView").send_keys("iphone")

#3rdTC:
driver.find_element_by_xpath("//andoid.widget.ImageButton[@context-desc='Drawer']").click()
driver.find_element_by_xpath("//andoid.widget.TextView[@text='Electronics']").click()
driver.find_element_by_xpath("//andoid.widget.TextView[@text='Laptops']").click()
driver.find_element_by_id("com.flipkart.android:id/tv_card_view_holder").click()

#4thTC:handling popup button
driver.find_element_by_id("com.flipkart.android:id/not_now_button").click()

#5thTC:verifying itemname
item_name = driver.find_element_by_xpath("//andoid.widget.TextView[@bounds='[226,475][339,509]']").get_attribute("text")
assert item_name=="Acer Aspire 7 Ryzen 5 Quad Core 3550H - (8 GB/512 GB","The price is not matched"

#6thTC:verifying price
price = driver.find_element_by_xpath("//andoid.widget.TextView[@bounds='[226,475][339,509]']").get_attribute("text")
assert price=="49,990","The price is not matched"

